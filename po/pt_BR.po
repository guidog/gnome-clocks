# Brazilian Portuguese translation for gnome-clocks.
# Copyright (C) 2023 gnome-clocks's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-clocks package.
# Djavan Fagundes <djavan@comum.org>, 2012.
# Edson Silva <edsonlead@gmail.com>, 2013.
# Fábio Nogueira <fnogueira@gnome.org>, 2016.
# Enrico Nicoletto <liverig@gmail.com>, 2012, 2013, 2014, 2016, 2020.
# Rafael Fontenelle <rafaelff@gnome.org>, 2017-2022.
# Leônidas Araújo <leorusvellt@hotmail.com>, 2022-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-clocks master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-clocks/issues\n"
"POT-Creation-Date: 2023-07-04 18:15+0000\n"
"PO-Revision-Date: 2023-04-12 07:46-0300\n"
"Last-Translator: Leônidas Araújo <leorusvellt@hotmail.com>\n"
"Language-Team: Brazilian Portuguese <https://br.gnome.org/traducao>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 42.0\n"
"X-Project-Style: gnome\n"
"X-DL-Team: pt_BR\n"
"X-DL-Module: gnome-clocks\n"
"X-DL-Branch: master\n"
"X-DL-Domain: po\n"
"X-DL-State: Translating\n"

#: data/org.gnome.clocks.desktop.in.in:3 data/org.gnome.clocks.desktop.in.in:4
#: data/org.gnome.clocks.metainfo.xml.in.in:6 data/ui/window.ui:25
#: src/main.vala:25 src/window.vala:286 src/window.vala:341
#: src/world-standalone.vala:24
msgid "Clocks"
msgstr "Relógios"

#: data/org.gnome.clocks.desktop.in.in:6
msgid "Clocks for world times, plus alarms, stopwatch and a timer"
msgstr ""
"Relógios para horários mundiais além de alarmes, cronômetro e um temporizador"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.clocks.desktop.in.in:8
msgid "time;timer;alarm;world clock;stopwatch;time zone;"
msgstr "hora;temporizador;alarme;relógio mundial;cronômetro;fuso horário;"

#: data/org.gnome.clocks.desktop.in.in:18
msgid "Allows world clocks to be displayed for your time zone."
msgstr "Permite que relógios mundiais sejam exibidos para o seu fuso horário."

#: data/org.gnome.clocks.gschema.xml.in:5
msgid "Configured world clocks"
msgstr "Relógios mundiais configurados"

#: data/org.gnome.clocks.gschema.xml.in:6
msgid "List of world clocks to show."
msgstr "Lista de relógios mundiais a serem mostrados."

#: data/org.gnome.clocks.gschema.xml.in:12
msgid "Configured alarms"
msgstr "Alarmes configurados"

#: data/org.gnome.clocks.gschema.xml.in:13
msgid "List of alarms set."
msgstr "Lista de alarmes ajustados."

#: data/org.gnome.clocks.gschema.xml.in:19
msgid "Configured Timers"
msgstr "Temporizadores configurados"

#: data/org.gnome.clocks.gschema.xml.in:20
msgid "List of timers set."
msgstr "Lista de temporizadores ajustados."

#: data/org.gnome.clocks.gschema.xml.in:26
msgid "Geolocation support"
msgstr "Suporte à localização geográfica"

#: data/org.gnome.clocks.gschema.xml.in:27
msgid "Turn geolocation support on and off."
msgstr "Habilita ou desabilita o suporte à localização geográfica."

#: data/org.gnome.clocks.gschema.xml.in:44
msgid "Window maximized"
msgstr "Janela maximizada"

#: data/org.gnome.clocks.gschema.xml.in:45
msgid "Whether the window is maximized."
msgstr "Se a janela está maximizada."

#: data/org.gnome.clocks.gschema.xml.in:51
msgid "Window width and height"
msgstr "Largura e altura da janela"

#: data/org.gnome.clocks.gschema.xml.in:52
msgid "Width and height of the window."
msgstr "Largura e altura da janela."

#: data/org.gnome.clocks.gschema.xml.in:58
msgid "Panel state"
msgstr "Estado do painel"

#: data/org.gnome.clocks.gschema.xml.in:59
msgid "Current clock panel."
msgstr "Painel do relógio atual."

#: data/org.gnome.clocks.metainfo.xml.in.in:7
msgid "Keep track of time"
msgstr "Mantenha rastro do tempo"

#: data/org.gnome.clocks.metainfo.xml.in.in:9
msgid ""
"A simple and elegant clock application. It includes world clocks, alarms, a "
"stopwatch, and timers."
msgstr ""
"Um aplicativo de relógio simples e elegante. Ele inclui relógios mundiais, "
"alarmes, cronômetro e um temporizador."

#: data/org.gnome.clocks.metainfo.xml.in.in:14
msgid "Show the time in different cities around the world"
msgstr "Mostra o horário de diferentes cidades ao redor do mundo"

#: data/org.gnome.clocks.metainfo.xml.in.in:15
msgid "Set alarms to wake you up"
msgstr "Define alarmes para lhe acordar"

#: data/org.gnome.clocks.metainfo.xml.in.in:16
msgid "Measure elapsed time with an accurate stopwatch"
msgstr "Mede o tempo gasto através de um cronômetro preciso"

#: data/org.gnome.clocks.metainfo.xml.in.in:17
msgid "Set timers to properly cook your food"
msgstr "Define temporizadores para que você cozinhe apropriadamente sua comida"

#: data/org.gnome.clocks.metainfo.xml.in.in:39
msgid "Initial screen"
msgstr "Tela inicial"

#: data/org.gnome.clocks.metainfo.xml.in.in:43
msgid "Alarms screen"
msgstr "Tela de alarmes"

#: data/org.gnome.clocks.metainfo.xml.in.in:47
msgid "Stopwatch screen"
msgstr "Tela de cronômetro"

#: data/org.gnome.clocks.metainfo.xml.in.in:51
msgid "Timer screen"
msgstr "Tela de temporizador"

#: data/org.gnome.clocks.metainfo.xml.in.in:136 src/window.vala:288
msgid "The GNOME Project"
msgstr "O Projeto GNOME"

#: data/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: data/gtk/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show Help"
msgstr "Mostra a ajuda"

#: data/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Open Menu"
msgstr "Abre o menu"

#: data/gtk/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Atalhos de teclado"

#: data/gtk/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sai"

#: data/gtk/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Go to the Next Section"
msgstr "Vai para a próxima seção"

#: data/gtk/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Go to the Previous Section"
msgstr "Vai para a seção anterior"

#: data/gtk/help-overlay.ui:51
msgctxt "shortcut window"
msgid "Go to the World Section"
msgstr "Vai para a seção de mundial"

#: data/gtk/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Go to the Alarms Section"
msgstr "Vai para a seção de alarmes"

#: data/gtk/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Go to the Stopwatch Section"
msgstr "Vai para a seção de cronômetro"

#: data/gtk/help-overlay.ui:69
msgctxt "shortcut window"
msgid "Go to the Timer Section"
msgstr "Vai para a seção de temporizador"

#: data/gtk/help-overlay.ui:77
msgctxt "shortcut window"
msgid "World Clocks"
msgstr "Relógios mundiais"

#: data/gtk/help-overlay.ui:81
msgctxt "shortcut window"
msgid "Add a World Clock"
msgstr "Adiciona um relógio mundial"

#: data/gtk/help-overlay.ui:89
msgctxt "shortcut window"
msgid "Alarm"
msgstr "Alarme"

#: data/gtk/help-overlay.ui:93
msgctxt "shortcut window"
msgid "Add an Alarm"
msgstr "Adiciona um alarme"

#: data/gtk/help-overlay.ui:101
msgctxt "shortcut window"
msgid "Stopwatch"
msgstr "Cronômetro"

#: data/gtk/help-overlay.ui:105
msgctxt "shortcut window"
msgid "Pause All"
msgstr "Pausa tudo"

#: data/gtk/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Timer"
msgstr "Temporizador"

#: data/gtk/help-overlay.ui:117
msgctxt "shortcut window"
msgid "Reset"
msgstr "Reinicia"

#: data/gtk/help-overlay.ui:123
msgctxt "shortcut window"
msgid "New Timer"
msgstr "Novo temporizador"

#: data/ui/alarm-day-picker-row.ui:17
msgid "Repeat"
msgstr "Repete"

#: data/ui/alarm-face.ui:14
msgid "Add A_larm…"
msgstr "Adicionar A_larme…"

#: data/ui/alarm-ringing-panel.ui:26 src/alarm-item.vala:128
msgid "Stop"
msgstr "Parar"

#: data/ui/alarm-ringing-panel.ui:41 src/alarm-item.vala:129
msgid "Snooze"
msgstr "Soneca"

#: data/ui/alarm-row.ui:47
msgid "Repeats"
msgstr "Repetições"

#: data/ui/alarm-row.ui:71 data/ui/timer-row.ui:152 data/ui/world-row.ui:19
msgid "Delete"
msgstr "Excluir"

#. Translators: Tooltip for the + button
#: data/ui/alarm-setup-dialog.ui:8 src/alarm-face.vala:28
#: src/alarm-setup-dialog.vala:113
msgid "New Alarm"
msgstr "Novo alarme"

#: data/ui/alarm-setup-dialog.ui:27 data/ui/timer-setup-dialog.ui:28
#: data/ui/world-location-dialog.ui:17
msgid "_Cancel"
msgstr "_Cancelar"

#: data/ui/alarm-setup-dialog.ui:34 data/ui/timer-setup-dialog.ui:35
#: data/ui/world-location-dialog.ui:25
msgid "_Add"
msgstr "_Adicionar"

#: data/ui/alarm-setup-dialog.ui:125
msgid "Name"
msgstr "Nome"

#: data/ui/alarm-setup-dialog.ui:132
msgid "Ring Duration"
msgstr "Duração do toque"

#: data/ui/alarm-setup-dialog.ui:138
msgid "Snooze Duration"
msgstr "Duração da soneca"

#: data/ui/alarm-setup-dialog.ui:150
msgid "You already have an alarm for this time."
msgstr "Você já possui um alarme para este horário."

#: data/ui/alarm-setup-dialog.ui:157
msgid "R_emove Alarm"
msgstr "R_emover alarme"

#: data/ui/header-bar.ui:6
msgid "_Keyboard Shortcuts"
msgstr "_Atalhos de teclado"

#: data/ui/header-bar.ui:10
msgid "_Help"
msgstr "_Ajuda"

#: data/ui/header-bar.ui:14
msgid "_About Clocks"
msgstr "_Sobre o relógios"

#: data/ui/header-bar.ui:54
msgid "Menu"
msgstr "Menu"

#: data/ui/header-bar.ui:57
msgid "Main Menu"
msgstr "Menu principal"

#: data/ui/stopwatch-face.ui:100
msgid "_Start"
msgstr "_Iniciar"

#: data/ui/stopwatch-face.ui:115 src/stopwatch-face.vala:184
msgid "Clear"
msgstr "Limpar"

#: data/ui/stopwatch-laps-row.ui:14
msgid "Time"
msgstr "Tempo"

#: data/ui/stopwatch-laps-row.ui:25
msgid "Difference"
msgstr "Diferença"

#: data/ui/timer-face.ui:21
msgid "Select Duration"
msgstr "Selecionar duração"

#: data/ui/timer-row.ui:32
msgid "Title…"
msgstr "Título…"

#: data/ui/timer-row.ui:74 src/stopwatch-face.vala:164
msgid "Pause"
msgstr "Pausar"

#: data/ui/timer-row.ui:92 src/stopwatch-face.vala:201
msgid "Start"
msgstr "Iniciar"

#: data/ui/timer-row.ui:119
msgid "Reset"
msgstr "Reiniciar"

#. Translators: Tooltip for the + button
#: data/ui/timer-setup-dialog.ui:6 src/timer-face.vala:39
msgid "New Timer"
msgstr "Novo temporizador"

#: data/ui/timer-setup.ui:38
msgid "1 m"
msgstr "1 m"

#: data/ui/timer-setup.ui:49
msgid "2 m"
msgstr "2 m"

#: data/ui/timer-setup.ui:60
msgid "3 m"
msgstr "3 m"

#: data/ui/timer-setup.ui:71
msgid "5 m"
msgstr "5 m"

#: data/ui/timer-setup.ui:82
msgid "30 m"
msgstr "30 m"

#: data/ui/timer-setup.ui:93
msgid "15 m"
msgstr "15 m"

#: data/ui/timer-setup.ui:104
msgid "45 m"
msgstr "45 m"

#: data/ui/timer-setup.ui:115
msgid "1 h"
msgstr "1 h"

#: data/ui/timer-setup.ui:147 data/ui/timer-setup.ui:179
#: data/ui/timer-setup.ui:212
msgid "0"
msgstr "0"

#: data/ui/window.ui:42
msgid "_World"
msgstr "_Mundial"

#: data/ui/window.ui:53
msgid "Ala_rms"
msgstr "Ala_rmes"

#: data/ui/window.ui:64
msgid "_Stopwatch"
msgstr "_Cronômetro"

#: data/ui/window.ui:75
msgid "_Timer"
msgstr "_Temporizador"

#: data/ui/window.ui:96
msgid "Clock"
msgstr "Relógio"

#: data/ui/window.ui:104
msgid "Alarm Ringing"
msgstr "Alarme tocando"

#: data/ui/world-face.ui:15
msgid "_Add World Clock…"
msgstr "_Adicionar relógio mundial…"

#: data/ui/world-location-dialog.ui:4
msgid "Add a New World Clock"
msgstr "Adicionar um novo relógio mundial"

#: data/ui/world-location-dialog.ui:53
msgid "Search for a City"
msgstr "Pesquise por uma cidade"

#: data/ui/world-standalone.ui:97
msgid "Sunrise"
msgstr "Nascer do sol"

#: data/ui/world-standalone.ui:109
msgid "Sunset"
msgstr "Pôr-do-sol"

#. Prior to 3.36 unamed alarms would just be called "Alarm",
#. pretend alarms called "Alarm" don't have a name (of course
#. this fails if the language/translation has since changed)
#: src/alarm-item.vala:125 src/alarm-row.vala:94 src/window.vala:337
msgid "Alarm"
msgstr "Alarme"

#. Translators: The alarm for the time %s titled %s has been "snoozed"
#: src/alarm-row.vala:101
#, c-format
msgid "Snoozed from %s: %s"
msgstr "Soneca de %s: %s"

#. Translators: %s is a time
#: src/alarm-row.vala:104
#, c-format
msgid "Snoozed from %s"
msgstr "Soneca de %s"

#: src/alarm-setup-dialog.vala:41
msgid "1 minute"
msgstr "1 minuto"

#: src/alarm-setup-dialog.vala:42
msgid "5 minutes"
msgstr "5 minutos"

#: src/alarm-setup-dialog.vala:43
msgid "10 minutes"
msgstr "10 minutos"

#: src/alarm-setup-dialog.vala:44
msgid "15 minutes"
msgstr "15 minutos"

#: src/alarm-setup-dialog.vala:45
msgid "20 minutes"
msgstr "20 minutos"

#: src/alarm-setup-dialog.vala:46
msgid "30 minutes"
msgstr "30 minutos"

#: src/alarm-setup-dialog.vala:113
msgid "Edit Alarm"
msgstr "Editar alarme"

#: src/alarm-setup-dialog.vala:116
msgid "_Done"
msgstr "C_oncluído"

#: src/application.vala:23
msgid "Print version information and exit"
msgstr "Exibe informações da versão e sai"

#: src/stopwatch-face.vala:168 src/stopwatch-face.vala:205
msgid "Lap"
msgstr "Volta"

#: src/stopwatch-face.vala:180
msgid "Resume"
msgstr "Continuar"

#: src/stopwatch-laps-row.vala:48
#, c-format
msgid "Lap %i"
msgstr "Volta %i"

#: src/timer-face.vala:76
msgid "Time is up!"
msgstr "Tempo esgotado!"

#: src/timer-face.vala:77
msgid "Timer countdown finished"
msgstr "Contagem regressiva do temporizador finalizada"

#. Translators: The time is the same as the local time
#: src/utils.vala:61
msgid "Current timezone"
msgstr "Fuso horário atual"

#. Translators: The (possibly fractical) number hours in the past
#. (relative to local) the clock/location is
#: src/utils.vala:66
#, c-format
msgid "%s hour behind"
msgid_plural "%s hours behind"
msgstr[0] "%s hora atrás"
msgstr[1] "%s horas atrás"

#. Translators: The (possibly fractical) number hours in the
#. future (relative to local) the clock/location is
#: src/utils.vala:72
#, c-format
msgid "%s hour ahead"
msgid_plural "%s hours ahead"
msgstr[0] "%s hora à frente"
msgstr[1] "%s horas à frente"

#. Translators: This is used in the repeat toggle for Monday
#: src/utils.vala:189
msgctxt "Alarm|Repeat-On|Monday"
msgid "M"
msgstr "S"

#. Translators: This is used in the repeat toggle for Tuesday
#: src/utils.vala:191
msgctxt "Alarm|Repeat-On|Tuesday"
msgid "T"
msgstr "T"

#. Translators: This is used in the repeat toggle for Wednesday
#: src/utils.vala:193
msgctxt "Alarm|Repeat-On|Wednesday"
msgid "W"
msgstr "Q"

#. Translators: This is used in the repeat toggle for Thursday
#: src/utils.vala:195
msgctxt "Alarm|Repeat-On|Thursday"
msgid "T"
msgstr "Q"

#. Translators: This is used in the repeat toggle for Friday
#: src/utils.vala:197
msgctxt "Alarm|Repeat-On|Friday"
msgid "F"
msgstr "S"

#. Translators: This is used in the repeat toggle for Saturday
#: src/utils.vala:199
msgctxt "Alarm|Repeat-On|Saturday"
msgid "S"
msgstr "S"

#. Translators: This is used in the repeat toggle for Sunday
#: src/utils.vala:201
msgctxt "Alarm|Repeat-On|Sunday"
msgid "S"
msgstr "D"

#: src/utils.vala:215
msgid "Mondays"
msgstr "Segundas-feiras"

#: src/utils.vala:216
msgid "Tuesdays"
msgstr "Terças-feiras"

#: src/utils.vala:217
msgid "Wednesdays"
msgstr "Quartas-feiras"

#: src/utils.vala:218
msgid "Thursdays"
msgstr "Quintas-feiras"

#: src/utils.vala:219
msgid "Fridays"
msgstr "Sextas-feiras"

#: src/utils.vala:220
msgid "Saturdays"
msgstr "Sábados"

#: src/utils.vala:221
msgid "Sundays"
msgstr "Domingos"

#: src/utils.vala:347
msgid "Every Day"
msgstr "Todo dia"

#: src/utils.vala:349
msgid "Weekdays"
msgstr "Dias da semana"

#: src/utils.vala:351
msgid "Weekends"
msgstr "Finais de semana"

#: src/window.vala:294
msgid "translator-credits"
msgstr ""
"Enrico Nicoletto <liverig@gmail.com>\n"
"Djavan Fagundes <djavan@comum.org>\n"
"Fábio Nogueira <fnogueira@gnome.org>\n"
"Rafael Fontenelle <rafaelff@gnome.org>\n"
"Leônidas Araújo <leorusvellt@hotmail.com>"

#. Translators: Tooltip for the + button
#: src/world-face.vala:29
msgid "Add Location"
msgstr "Adiciona localização"

#. If it is Jan 1st there, and not Jan 2nd here, then it must be
#. Dec 31st here, so return "tomorrow"
#: src/world-item.vala:177 src/world-item.vala:181
msgid "Tomorrow"
msgstr "Amanhã"

#. If it is Jan 1st here, and not Jan 2nd there, then it must be
#. Dec 31st there, so return "yesterday"
#: src/world-item.vala:177 src/world-item.vala:181
msgid "Yesterday"
msgstr "Ontem"

#. Translators: This clock represents the local time
#: src/world-row.vala:47
msgid "Current location"
msgstr "Localização atual"

#~ msgid "Back"
#~ msgstr "Voltar"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Add"
#~ msgstr "Adicionar"

#~ msgid "Active"
#~ msgstr "Ativo"

#~ msgid "Alarms"
#~ msgstr "Alarmes"

#~ msgid "Goals:"
#~ msgstr "Finalidades:"

#~ msgid "Alarms setup"
#~ msgstr "Configuração de alarmes"

#~ msgid "Optional"
#~ msgstr "Opcional"

#~ msgid "Utilities to help you with the time."
#~ msgstr "Utilitários para ajudá-lo com o tempo."

#~ msgid "GNOME Clocks"
#~ msgstr "Relógios do GNOME"

#~ msgctxt "shortcut window"
#~ msgid "Keyboard shortcuts"
#~ msgstr "Atalhos de teclado"

#~ msgctxt "shortcut window"
#~ msgid "Add a world clock"
#~ msgstr "Adicionar um relógio mundial"

#~ msgctxt "shortcut window"
#~ msgid "Stop / Reset"
#~ msgstr "Parar / Reiniciar"

#~ msgid "Failed to show help: %s"
#~ msgstr "Ocorreu falha ao mostrar ajuda: %s"

#~ msgid "Edit"
#~ msgstr "Editar"

#~ msgid "Select <b>New</b> to add a world clock"
#~ msgstr "Selecione <b>Novo</b> para adicionar um relógio mundial"

#~ msgid "%i second timer"
#~ msgstr "Temporizador de %i segundo(s)"

#~ msgid "%i hour timer"
#~ msgstr "Temporizador de %i hora(s)"

#~ msgid "%i minute %i second timer"
#~ msgstr "Temporizador de %i minuto(s) %i segundo(s)"

#~ msgid "%i hour %i minute timer"
#~ msgstr "Temporizador de %i hora(s) %i minuto(s)"

#~ msgid "%i hour %i second timer"
#~ msgstr "Temporizador de %i hora(s) %i segundo(s)"

#~ msgid "%i hour %i minute %i second timer"
#~ msgstr "Temporizador de %i hora(s) %i minute(s) %i segundo(s)"

#~ msgctxt "Repeat|Thursday"
#~ msgid "T"
#~ msgstr "Q"

#~ msgctxt "Repeat|Sunday"
#~ msgid "S"
#~ msgstr "D"

#~ msgctxt "shortcut window"
#~ msgid "Select all world clocks"
#~ msgstr "Selecionar todos os relógios mundiais"

#~ msgctxt "shortcut window"
#~ msgid "Select all alarms"
#~ msgstr "Selecionar todos os alarmes"

#~ msgid "Select All"
#~ msgstr "Selecionar tudo"

#~ msgid "Select None"
#~ msgstr "Desmarcar tudo"

#~ msgid "Select <b>New</b> to add an alarm"
#~ msgstr "Selecione <b>Novo</b> para adicionar um alarme"

#~ msgid "Select"
#~ msgstr "Selecionar"

#~ msgid "Click on items to select them"
#~ msgstr "Clique sobre os itens para selecioná-los"

#~ msgid "%u selected"
#~ msgid_plural "%u selected"
#~ msgstr[0] "%u selecionado"
#~ msgstr[1] "%u selecionados"

#~ msgid "Configured timer duration in seconds."
#~ msgstr "Duração configurada do temporizador em segundos."

#~ msgid "Continue"
#~ msgstr "Continuar"

#~ msgid "@icon@"
#~ msgstr "@icon@"

#~ msgctxt "Alarm"
#~ msgid "New"
#~ msgstr "Novo"

#~ msgctxt "World clock"
#~ msgid "New"
#~ msgstr "Novo"

#~ msgid "_About"
#~ msgstr "_Sobre"

#~ msgid "_Quit"
#~ msgstr "Sa_ir"

#~ msgid "org.gnome.clocks"
#~ msgstr "org.gnome.clocks"

#~ msgid "_New"
#~ msgstr "_Novo"
